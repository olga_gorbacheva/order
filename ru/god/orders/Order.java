package ru.god.orders;

import java.io.*;

/**
 * Класс, реализующий чтение данных из файла, работу с ними и запись новых данных в другой файл
 *
 * @author Горбачева, 16ИТ18к
 */
public class Order {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\text.txt"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\newText.txt"))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                String[] array = string.split(" ");
                int time = Integer.parseInt(bufferedReader.readLine());
                int[] newArray = new int[array.length];
                for (int i = 0; i < array.length; i++) {
                    newArray[i] = Integer.parseInt(array[i]);
                }
                if (time >= array.length) {
                    string = String.valueOf(sum(newArray) + "\n");
                    bufferedWriter.write(string);
                } else {
                    newArray = sortedArray(newArray);
                    string = String.valueOf(specialSum(newArray, time) + "\n");
                    bufferedWriter.write(string);
                }
            }
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, позволяющий отсортировать элементы массива по убыванию
     *
     * @param array - исходный массив
     * @return отсортированный массив
     */
    private static int[] sortedArray(int[] array) {
        int buffer;
        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array.length - j - 1; i++) {
                if (array[i] < array[i + 1]) {
                    buffer = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buffer;
                }
            }
        }
        return array;
    }

    /**
     * Метод, позволяющий вычислить сумму всех элементов массива
     *
     * @param array - массив элементов
     * @return сумма всех элементов массива
     */
    private static int sum(int[] array) {
        int sum = 0;
        for (int element : array) {
            sum += element;
        }
        return sum;
    }

    /**
     * Метод, позволяющий вычислить сумму определенного количества элементов массива
     *
     * @param array - массив элементов
     * @param index - количество элементов, которые нужно сложить, от начала массива
     * @return сумма элементов
     */
    private static int specialSum(int[] array, int index) {
        int sum = 0;
        for (int i = 0; i < index; i++) {
            sum += array[i];
        }
        return sum;
    }
}
